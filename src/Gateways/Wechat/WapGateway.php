<?php

namespace Szby\Pay\Gateways\Wechat;

use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Szby\Pay\Log;

class WapGateway extends Gateway
{
    /**
     * Pay an order.
     *
     * @author Kevin <kevin@100yiyou.com>
     *
     * @param string $endpoint
     * @param array  $payload
     *
     * @throws \Szby\Pay\Exceptions\GatewayException
     * @throws \Szby\Pay\Exceptions\InvalidArgumentException
     * @throws \Szby\Pay\Exceptions\InvalidSignException
     *
     * @return Response
     */
    public function pay($endpoint, array $payload)
    {
        $payload['trade_type'] = $this->getTradeType();

        Log::info('Starting To Pay A Wechat Wap Order', [$endpoint, $payload]);

        $data = $this->preOrder($payload);

        $url = is_null(Support::getInstance()->return_url) ? $data->mweb_url : $data->mweb_url.
                        '&redirect_url='.urlencode(Support::getInstance()->return_url);

        return RedirectResponse::create($url);
    }

    /**
     * Get trade type config.
     *
     * @author Kevin <kevin@100yiyou.com>
     *
     * @return string
     */
    protected function getTradeType()
    {
        return 'MWEB';
    }
}
