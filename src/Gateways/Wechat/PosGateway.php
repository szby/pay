<?php

namespace Szby\Pay\Gateways\Wechat;

use Szby\Pay\Log;
use Szby\Pay\Collection;

class PosGateway extends Gateway
{
    /**
     * Pay an order.
     *
     * @author Kevin <kevin@100yiyou.com>
     *
     * @param string $endpoint
     * @param array  $payload
     *
     * @throws \Szby\Pay\Exceptions\GatewayException
     * @throws \Szby\Pay\Exceptions\InvalidArgumentException
     * @throws \Szby\Pay\Exceptions\InvalidSignException
     *
     * @return Collection
     */
    public function pay($endpoint, array $payload)
    {
        unset($payload['trade_type'], $payload['notify_url']);

        $payload['sign'] = Support::generateSign($payload);

        Log::info('Starting To Pay A Wechat Pos order', [$payload]);

        return Support::requestApi('pay/micropay', $payload);
    }

    /**
     * Get trade type config.
     *
     * @author Kevin <kevin@100yiyou.com>
     *
     * @return string
     */
    protected function getTradeType()
    {
        return 'MICROPAY';
    }
}
