<?php

namespace Szby\Pay\Gateways\Wechat;

use Symfony\Component\HttpFoundation\Request;
use Szby\Pay\Gateways\Wechat;
use Szby\Pay\Log;
use Szby\Pay\Collection;

class TransferGateway extends Gateway
{
    /**
     * Pay an order.
     *
     * @author Kevin <kevin@100yiyou.com>
     *
     * @param string $endpoint
     * @param array  $payload
     *
     * @throws \Szby\Pay\Exceptions\GatewayException
     * @throws \Szby\Pay\Exceptions\InvalidArgumentException
     * @throws \Szby\Pay\Exceptions\InvalidSignException
     *
     * @return Collection
     */
    public function pay($endpoint, array $payload)
    {
        if ($this->mode === Wechat::MODE_SERVICE) {
            unset($payload['sub_mch_id'], $payload['sub_appid']);
        }

        $type = Support::getInstance()->getTypeName($payload['type']);

        $payload['mch_appid'] = Support::getInstance()->getConfig($type, '');
        $payload['mchid'] = $payload['mch_id'];

        if (php_sapi_name() !== 'cli') {
            $payload['spbill_create_ip'] = Request::createFromGlobals()->server->get('SERVER_ADDR');
        }

        unset($payload['appid'], $payload['mch_id'], $payload['trade_type'],
            $payload['notify_url'], $payload['type']);

        $payload['sign'] = Support::generateSign($payload);

        Log::info('Starting To Pay A Wechat Transfer Order', [$endpoint, $payload]);

        return Support::requestApi(
            'mmpaymkttransfers/promotion/transfers',
            $payload,
            true
        );
    }

    /**
     * Get trade type config.
     *
     * @author Kevin <kevin@100yiyou.com>
     *
     * @return string
     */
    protected function getTradeType()
    {
        return '';
    }
}
