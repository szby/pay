<?php

namespace Szby\Pay\Gateways\Wechat;

use Szby\Pay\Contracts\GatewayInterface;
use Szby\Pay\Log;
use Szby\Pay\Collection;
use Szby\Pay\Config;

abstract class Gateway implements GatewayInterface
{
    /**
     * Mode.
     *
     * @var string
     */
    protected $mode;

    /**
     * Bootstrap.
     *
     * @author Kevin <kevin@100yiyou.com>
     *
     * @throws \Szby\Pay\Exceptions\InvalidArgumentException
     */
    public function __construct()
    {
        $this->mode = Support::getInstance()->mode;
    }

    /**
     * Pay an order.
     *
     * @author Kevin <kevin@100yiyou.com>
     *
     * @param string $endpoint
     * @param array  $payload
     *
     * @return Collection
     */
    abstract public function pay($endpoint, array $payload);

    /**
     * Get trade type config.
     *
     * @author Kevin <kevin@100yiyou.com>
     *
     * @return string
     */
    abstract protected function getTradeType();

    /**
     * Schedule an order.
     *
     * @author Kevin <kevin@100yiyou.com>
     *
     * @param array $payload
     *
     * @throws \Szby\Pay\Exceptions\GatewayException
     * @throws \Szby\Pay\Exceptions\InvalidArgumentException
     * @throws \Szby\Pay\Exceptions\InvalidSignException
     *
     * @return Collection
     */
    protected function preOrder($payload)
    {
        $payload['sign'] = Support::generateSign($payload);

        Log::debug('Schedule A Wechat order', [$payload]);

        return Support::requestApi('pay/unifiedorder', $payload);
    }
}
