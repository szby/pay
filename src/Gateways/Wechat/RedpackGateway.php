<?php

namespace Szby\Pay\Gateways\Wechat;

use Szby\Component\HttpFoundation\Request;
use Szby\Pay\Gateways\Wechat;
use Szby\Pay\Log;
use Szby\Pay\Collection;

class RedpackGateway extends Gateway
{
    /**
     * Pay an order.
     *
     * @author Kevin <kevin@100yiyou.com>
     *
     * @param string $endpoint
     * @param array  $payload
     *
     * @throws \Szby\Pay\Exceptions\GatewayException
     * @throws \Szby\Pay\Exceptions\InvalidArgumentException
     * @throws \Szby\Pay\Exceptions\InvalidSignException
     *
     * @return Collection
     */
    public function pay($endpoint, array $payload)
    {
        $payload['wxappid'] = $payload['appid'];

        if (php_sapi_name() !== 'cli') {
            $payload['client_ip'] = Request::createFromGlobals()->server->get('SERVER_ADDR');
        }

        if ($this->mode !== Wechat::MODE_SERVICE) {
            $payload['msgappid'] = $payload['appid'];
        }

        unset($payload['appid'], $payload['trade_type'],
              $payload['notify_url'], $payload['spbill_create_ip']);

        $payload['sign'] = Support::generateSign($payload);

        Log::info('Starting To Pay A Wechat Redpack Order', [$endpoint, $payload]);

        return Support::requestApi(
            'mmpaymkttransfers/sendredpack',
            $payload,
            true
        );
    }

    /**
     * Get trade type config.
     *
     * @author Kevin <kevin@100yiyou.com>
     *
     * @return string
     */
    protected function getTradeType()
    {
        return '';
    }
}
