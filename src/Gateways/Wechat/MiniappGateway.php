<?php

namespace Szby\Pay\Gateways\Wechat;

use Szby\Pay\Gateways\Wechat;
use Szby\Pay\Collection;

class MiniappGateway extends MpGateway
{
    /**
     * Pay an order.
     *
     * @author Kevin <kevin@100yiyou.com>
     *
     * @param string $endpoint
     * @param array  $payload
     *
     * @throws \Szby\Pay\Exceptions\GatewayException
     * @throws \Szby\Pay\Exceptions\InvalidArgumentException
     * @throws \Szby\Pay\Exceptions\InvalidSignException
     *
     * @return Collection
     */
    public function pay($endpoint, array $payload)
    {
        $payload['appid'] = Support::getInstance()->miniapp_id;

        if ($this->mode !== Wechat::MODE_SERVICE) {
            $payload['sub_appid'] = Support::getInstance()->sub_miniapp_id;
        }

        return parent::pay($endpoint, $payload);
    }
}
