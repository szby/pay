<?php

namespace Szby\Pay\Gateways\Wechat;

use Szby\Pay\Log;
use Szby\Pay\Collection;
use Szby\Pay\Str;

class MpGateway extends Gateway
{
    /**
     * Pay an order.
     *
     * @author Kevin <kevin@100yiyou.com>
     *
     * @param string $endpoint
     * @param array  $payload
     *
     * @throws \Szby\Pay\Exceptions\GatewayException
     * @throws \Szby\Pay\Exceptions\InvalidArgumentException
     * @throws \Szby\Pay\Exceptions\InvalidSignException
     * @throws \Exception
     *
     * @return Collection
     */
    public function pay($endpoint, array $payload)
    {
        $payload['trade_type'] = $this->getTradeType();

        $payRequest = [
            'appId'     => $payload['appid'],
            'timeStamp' => strval(time()),
            'nonceStr'  => Str::random(),
            'package'   => 'prepay_id='.$this->preOrder($payload)->prepay_id,
            'signType'  => 'HMAC-SHA256',
        ];
        $payRequest['paySign'] = Support::generateSign($payRequest);

        Log::info('Starting To Pay A Wechat JSAPI Order', [$endpoint, $payRequest]);

        return new Collection($payRequest);
    }

    /**
     * Get trade type config.
     *
     * @author Kevin <kevin@100yiyou.com>
     *
     * @return string
     */
    protected function getTradeType()
    {
        return 'JSAPI';
    }
}
