<?php

namespace Szby\Pay\Gateways;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Szby\Pay\Contracts\GatewayApplicationInterface;
use Szby\Pay\Contracts\GatewayInterface;
use Szby\Pay\Exceptions\InvalidGatewayException;
use Szby\Pay\Exceptions\InvalidSignException;
use Szby\Pay\Gateways\Alipay\Support;
use Szby\Pay\Log;
use Szby\Pay\Collection;
use Szby\Pay\Config;
use Szby\Pay\Str;

/**
 * @method Response app(array $config) APP 支付
 * @method Collection pos(array $config) 刷卡支付
 * @method Collection scan(array $config) 扫码支付
 * @method Collection transfer(array $config) 帐户转账
 * @method Response wap(array $config) 手机网站支付
 * @method Response web(array $config) 电脑支付
 */
class Alipay implements GatewayApplicationInterface
{
    /**
     * Const mode_normal.
     */
    const MODE_NORMAL = 'normal';

    /**
     * Const mode_dev.
     */
    const MODE_DEV = 'dev';

    /**
     * Const url.
     */
    const URL = [
        self::MODE_NORMAL => 'https://openapi.alipay.com/gateway.do',
        self::MODE_DEV    => 'https://openapi.alipaydev.com/gateway.do',
    ];

    /**
     * Alipay payload.
     *
     * @var array
     */
    protected $payload;

    /**
     * Alipay gateway.
     *
     * @var string
     */
    protected $gateway;

    /**
     * Bootstrap.
     *
     * @author Kevin <kevin@100yiyou.com>
     *
     * @param Config $config
     *
     * @throws \Szby\Pay\Exceptions\InvalidArgumentException
     */
    public function __construct(Config $config)
    {
        $this->gateway = Support::getInstance($config)->getBaseUri();
        $this->payload = [
            'app_id'      => $config->get('app_id'),
            'method'      => '',
            'format'      => 'JSON',
            'charset'     => 'utf-8',
            'sign_type'   => 'RSA2',
            'version'     => '1.0',
            'return_url'  => $config->get('return_url'),
            'notify_url'  => $config->get('notify_url'),
            'timestamp'   => date('Y-m-d H:i:s'),
            'sign'        => '',
            'biz_content' => '',
        ];
    }

    /**
     * Magic pay.
     *
     * @author Kevin <kevin@100yiyou.com>
     *
     * @param string $method
     * @param array  $params
     *
     * @throws InvalidGatewayException
     *
     * @return Response|Collection
     */
    public function __call($method, $params)
    {
        return $this->pay($method, ...$params);
    }

    /**
     * Pay an order.
     *
     * @author Kevin <kevin@100yiyou.com>
     *
     * @param string $gateway
     * @param array  $params
     *
     * @throws InvalidGatewayException
     *
     * @return Response|Collection
     */
    public function pay($gateway, $params = [])
    {
        Log::debug('Starting To Alipay', [$gateway, $params]);

        $this->payload['return_url'] = $params['return_url'] ?? $this->payload['return_url'];
        $this->payload['notify_url'] = $params['notify_url'] ?? $this->payload['notify_url'];

        unset($params['return_url'], $params['notify_url']);

        $this->payload['biz_content'] = json_encode($params);

        $gateway = get_class($this).'\\'.Str::studly($gateway).'Gateway';

        if (class_exists($gateway)) {
            return $this->makePay($gateway);
        }

        throw new InvalidGatewayException("Pay Gateway [{$gateway}] not exists");
    }

    /**
     * Verify sign.
     *
     * @author Kevin <kevin@100yiyou.com>
     *
     * @param null|array $data
     * @param bool       $refund
     *
     * @throws InvalidSignException
     * @throws \Szby\Pay\Exceptions\InvalidArgumentException
     * @throws \Szby\Pay\Exceptions\InvalidConfigException
     *
     * @return Collection
     */
    public function verify($data = null, $refund = false)
    {
        if (is_null($data)) {
            $request = Request::createFromGlobals();

            $data = $request->request->count() > 0 ? $request->request->all() : $request->query->all();
            $data = Support::encoding($data, 'utf-8', $data['charset'] ?? 'gb2312');
        }

        Log::info('Received Alipay Request', $data);

        if (Support::verifySign($data)) {
            return new Collection($data);
        }

        Log::warning('Alipay Sign Verify FAILED', $data);

        throw new InvalidSignException('Alipay Sign Verify FAILED', $data);
    }

    /**
     * Query an order.
     *
     * @author Kevin <kevin@100yiyou.com>
     *
     * @param string|array $order
     * @param bool         $refund
     *
     * @throws InvalidSignException
     * @throws \Szby\Pay\Exceptions\GatewayException
     * @throws \Szby\Pay\Exceptions\InvalidConfigException
     * @throws \Szby\Pay\Exceptions\InvalidArgumentException
     *
     * @return Collection
     */
    public function find($order, $refund = false)
    {
        $this->payload['method'] = $refund ? 'alipay.trade.fastpay.refund.query' : 'alipay.trade.query';
        $this->payload['biz_content'] = json_encode(is_array($order) ? $order : ['out_trade_no' => $order]);
        $this->payload['sign'] = Support::generateSign($this->payload);

        Log::info('Starting To Find An Alipay Order', [$this->gateway, $this->payload]);

        return Support::requestApi($this->payload);
    }

    /**
     * Refund an order.
     *
     * @author Kevin <kevin@100yiyou.com>
     *
     * @param array $order
     *
     * @throws InvalidSignException
     * @throws \Szby\Pay\Exceptions\GatewayException
     * @throws \Szby\Pay\Exceptions\InvalidConfigException
     * @throws \Szby\Pay\Exceptions\InvalidArgumentException
     *
     * @return Collection
     */
    public function refund($order)
    {
        $this->payload['method'] = 'alipay.trade.refund';
        $this->payload['biz_content'] = json_encode($order);
        $this->payload['sign'] = Support::generateSign($this->payload);

        Log::info('Starting To Refund An Alipay Order', [$this->gateway, $this->payload]);

        return Support::requestApi($this->payload);
    }

    /**
     * Cancel an order.
     *
     * @author Kevin <kevin@100yiyou.com>
     *
     * @param string|array $order
     *
     * @throws InvalidSignException
     * @throws \Szby\Pay\Exceptions\GatewayException
     * @throws \Szby\Pay\Exceptions\InvalidConfigException
     * @throws \Szby\Pay\Exceptions\InvalidArgumentException
     *
     * @return Collection
     */
    public function cancel($order)
    {
        $this->payload['method'] = 'alipay.trade.cancel';
        $this->payload['biz_content'] = json_encode(is_array($order) ? $order : ['out_trade_no' => $order]);
        $this->payload['sign'] = Support::generateSign($this->payload);

        Log::info('Starting To Cancel An Alipay Order', [$this->gateway, $this->payload]);

        return Support::requestApi($this->payload);
    }

    /**
     * Close an order.
     *
     * @author Kevin <kevin@100yiyou.com>
     *
     * @param string|array $order
     *
     * @throws InvalidSignException
     * @throws \Szby\Pay\Exceptions\GatewayException
     * @throws \Szby\Pay\Exceptions\InvalidArgumentException
     * @throws \Szby\Pay\Exceptions\InvalidConfigException
     *
     * @return Collection
     */
    public function close($order)
    {
        $this->payload['method'] = 'alipay.trade.close';
        $this->payload['biz_content'] = json_encode(is_array($order) ? $order : ['out_trade_no' => $order]);
        $this->payload['sign'] = Support::generateSign($this->payload);

        Log::info('Starting To Close An Alipay Order', [$this->gateway, $this->payload]);

        return Support::requestApi($this->payload);
    }

    /**
     * Download bill.
     *
     * @author Kevin <kevin@100yiyou.com>
     *
     * @param string|array $bill
     *
     * @throws InvalidSignException
     * @throws \Szby\Pay\Exceptions\GatewayException
     * @throws \Szby\Pay\Exceptions\InvalidArgumentException
     * @throws \Szby\Pay\Exceptions\InvalidConfigException
     *
     * @return string
     */
    public function download($bill)
    {
        $this->payload['method'] = 'alipay.data.dataservice.bill.downloadurl.query';
        $this->payload['biz_content'] = json_encode(is_array($bill) ? $bill : ['bill_type' => 'trade', 'bill_date' => $bill]);
        $this->payload['sign'] = Support::generateSign($this->payload);

        Log::info('Starting To Download The Alipay Bill', [$this->gateway, $this->payload]);

        $result = Support::requestApi($this->payload);

        return ($result instanceof Collection) ? $result->bill_download_url : '';
    }

    /**
     * Reply success to alipay.
     *
     * @author Kevin <kevin@100yiyou.com>
     *
     * @return Response
     */
    public function success()
    {
        return Response::create('success');
    }

    /**
     * Make pay gateway.
     *
     * @author Kevin <kevin@100yiyou.com>
     *
     * @param string $gateway
     *
     * @throws InvalidGatewayException
     *
     * @return Response|Collection
     */
    protected function makePay($gateway)
    {
        $app = new $gateway();

        if ($app instanceof GatewayInterface) {
            return $app->pay($this->gateway, $this->payload);
        }

        throw new InvalidGatewayException("Pay Gateway [{$gateway}] Must Be An Instance Of GatewayInterface");
    }
}
