<?php

namespace Szby\Pay\Exceptions;

class InvalidArgumentException extends Exception
{
    /**
     * Bootstrap.
     *
     * @author Kevin <kevin@100yiyou.com>
     *
     * @param string       $message
     * @param array|string $raw
     * @param int|string   $code
     */
    public function __construct($message, $raw = [], $code = 3)
    {
        parent::__construct($message, $raw, $code);
    }
}
