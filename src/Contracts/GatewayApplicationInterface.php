<?php

namespace Szby\Pay\Contracts;

interface GatewayApplicationInterface
{
    /**
     * To pay.
     *
     * @author Kevin <kevin@100yiyou.com>
     *
     * @param string $gateway
     * @param array  $params
     *
     * @return \Szby\Pay\Collection|\Symfony\Component\HttpFoundation\Response
     */
    public function pay($gateway, $params);

    /**
     * Query an order.
     *
     * @author Kevin <kevin@100yiyou.com>
     *
     * @param string|array $order
     * @param bool         $refund
     *
     * @return \Szby\Pay\Collection
     */
    public function find($order, $refund);

    /**
     * Refund an order.
     *
     * @author Kevin <kevin@100yiyou.com>
     *
     * @param array $order
     *
     * @return \Szby\Pay\Collection
     */
    public function refund($order);

    /**
     * Cancel an order.
     *
     * @author Kevin <kevin@100yiyou.com>
     *
     * @param string|array $order
     *
     * @return \Szby\Pay\Collection
     */
    public function cancel($order);

    /**
     * Close an order.
     *
     * @author Kevin <kevin@100yiyou.com>
     *
     * @param string|array $order
     *
     * @return \Szby\Pay\Collection
     */
    public function close($order);

    /**
     * Verify a request.
     *
     * @author Kevin <kevin@100yiyou.com>
     *
     * @param string|null $content
     * @param bool        $refund
     *
     * @return \Szby\Pay\Collection
     */
    public function verify($content, $refund);

    /**
     * Echo success to server.
     *
     * @author Kevin <kevin@100yiyou.com>
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function success();
}
