<?php

namespace Szby\Pay\Contracts;

interface GatewayInterface
{
    /**
     * Pay an order.
     *
     * @author Kevin <kevin@100yiyou.com>
     *
     * @param string $endpoint
     * @param array  $payload
     *
     * @return \Szby\Pay\Collection|\Symfony\Component\HttpFoundation\Response
     */
    public function pay($endpoint, array $payload);
}
